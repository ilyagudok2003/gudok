# Вводим анимацию в консоль

import time
array = []  # Хранение кадров



file = open('earth.md', 'r')# Открытие файла 
string = ''.join([line for line in file])  # Извлечение всех символов
frameNum = 1  # Номер одного кадра



while frameNum < 26:
    frame = string.split(f'\n```\n')  # Деление кадров
    array.append(str(frame[frameNum]))  # Добавление кадров массив (общий)
    frameNum += 1
print('\n' * 100)



# Вывод анимации 3 раза
for j in range(1, 3):
    for i in range(0, len(array)):
        if i % 2 == 0:
            print(f'\033[96m{array[i]}\033[0m')  # Вывод кадров (цветных)
            time.sleep(1)
            print('\n' * 100)

file.close()
