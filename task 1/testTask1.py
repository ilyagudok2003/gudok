e = 42
file = open("file.txt", "r") # Открытие файла

# Сложение строк из файла
while True:
    line = file.readline()  # Полученные строки
    if not line:
        break

    e += 1 / int(line)  # Производим вычисления 

file.close()
print(f'Сумма ряда: {e}')

